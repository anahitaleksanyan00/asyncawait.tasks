﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using AsyncAwait.Task2.CodeReviewChallenge.Headers;
using CloudServices.Interfaces;
using Microsoft.AspNetCore.Http;

namespace AsyncAwait.Task2.CodeReviewChallenge.Middleware;

public class StatisticMiddleware
{
    private readonly RequestDelegate _next;

    private readonly IStatisticService _statisticService;

    public StatisticMiddleware(RequestDelegate next, IStatisticService statisticService)
    {
        _next = next;
        _statisticService = statisticService ?? throw new ArgumentNullException(nameof(statisticService));
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (VisitorsCount.initTask != null && !VisitorsCount.initTask.IsCompleted)
        {
            await VisitorsCount.initTask;
        }

        string path = context.Request.Path;

        VisitorsCount.countTask = Task.Run(async () =>
        {
            var result = await _statisticService.GetVisitsCountAsync(path) + 1;
            return result.ToString();
        });

        VisitorsCount.initTask = Task.Run(async () =>
        {
            await _statisticService.RegisterVisitAsync(path);
            return true;
        });

        await _next(context);
    }
}

public class VisitorsCount
{
    public static Task<string> countTask;
    public static Task<bool> initTask;
}
